from django.contrib import admin

# Register your models here.
from django.contrib import admin

from .models import Pais, Departamento, Municipio

class PaisAdmin(admin.ModelAdmin):
  list_display = ('id', 'nombre')

admin.site.register(Pais, PaisAdmin)

class DepartamentoAdmin(admin.ModelAdmin):
  list_display = ('id', 'nombre', 'pais')
  search_fields = ('nombre', 'pais__nombre')


admin.site.register(Departamento, DepartamentoAdmin)

class MunicipioAdmin(admin.ModelAdmin):
  list_display = ('id', 'nombre', 'departamento')
  search_fields = ('nombre', 'municipio__nombre')


admin.site.register(Municipio, MunicipioAdmin)