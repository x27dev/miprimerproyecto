from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from rest_framework import generics
from .models import Pais, Departamento, Municipio
from .serializers import PaisSerializer, DepartamentoSerializer, MunicipioSerializer
from .forms import PaisForm, DepartamentoForm, MunicipioForm
from django.contrib.auth.decorators import permission_required

#vistas

class PaisView(generics.ListCreateAPIView):
  queryset = Pais.objects.all()
  serializer_class = PaisSerializer  # Agrega esta línea
class DepartamentoView(generics.ListCreateAPIView):
  queryset = Departamento.objects.all()
  serializer_class = DepartamentoSerializer  # Agrega esta línea
class MunicipioView(generics.ListCreateAPIView):
  queryset = Municipio.objects.all()
  serializer_class = MunicipioSerializer  # Agrega esta línea


#ver todos juntos
@permission_required('catalogos.view_pais', raise_exception=True)
def catalogos(request):
    paises = Pais.objects.all()
    departamentos = Departamento.objects.all()
    municipios = Municipio.objects.all()

    return render(request, 'catalogos/catalogos.html', {'paises': paises, 'departamentos': departamentos, 'municipios': municipios})


#Crear
@permission_required('catalogos.add_pais', raise_exception=True)
def crear_pais(request):
    if request.method == 'POST':
        form = PaisForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('catalogos')
    else:
        form = PaisForm()
    return render(request, 'catalogos/crear.html', {'form': form})
def crear_departamento(request):
    if request.method == 'POST':
        form = DepartamentoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('catalogos')
    else:
        form = DepartamentoForm()
    return render(request, 'catalogos/crear.html', {'form': form})
def crear_municipio(request):
    if request.method == 'POST':
        form = MunicipioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('catalogos')
    else:
        form = MunicipioForm()
    return render(request, 'catalogos/crear.html', {'form': form})


#editar
@permission_required('catalogos.change_pais', raise_exception=True)
def editar_pais(request, pais_id):
    pais = get_object_or_404(Pais, pk=pais_id)

    if request.method == 'POST':
        form = PaisForm(request.POST, instance=pais)
        if form.is_valid():
            form.save()
            return redirect('catalogos')  # Redirige a la lista de países después de editar
    else:
        form = PaisForm(instance=pais)

    return render(request, 'catalogos/editar.html', {'form': form, 'pais': pais})
def editar_departamento(request, departamento_id):
    departamento = get_object_or_404(Departamento, pk=departamento_id)

    if request.method == 'POST':
        form = DepartamentoForm(request.POST, instance=departamento)
        if form.is_valid():
            form.save()
            return redirect('catalogos')  # Redirige a la lista de países después de editar
    else:
        form = DepartamentoForm(instance=departamento)

    return render(request, 'catalogos/editar.html', {'form': form, 'departamento': departamento})
def editar_municipio(request, municipio_id):
    municipio = get_object_or_404(Municipio, pk=municipio_id)

    if request.method == 'POST':
        form = MunicipioForm(request.POST, instance=municipio)
        if form.is_valid():
            form.save()
            return redirect('catalogos')  # Redirige a la lista de países después de editar
    else:
        form = MunicipioForm(instance=municipio)

    return render(request, 'catalogos/editar.html', {'form': form, 'municipio': municipio})


#eliminar
@permission_required('catalogos.delete_pais', raise_exception=True)
def eliminar_pais(request, pais_id):
    try:
        eliminar = Pais.objects.get(pk=pais_id)
        if request.method == 'POST':
            eliminar.delete()
            return redirect('catalogos')  # Redirige a la lista de países
        return render(request, 'catalogos/eliminar.html', {'pais': eliminar})
    except Pais.DoesNotExist:
        return redirect('catalogos')
def eliminar_departamento(request, departamento_id):
    try:
        eliminar = Departamento.objects.get(pk=departamento_id)
        if request.method == 'POST':
            eliminar.delete()
            return redirect('catalogos')  # Redirige a la lista de departamentos
        return render(request, 'catalogos/eliminar.html', {'departamento': eliminar})
    except Departamento.DoesNotExist:
        return redirect('catalogos')
def eliminar_municipio(request, municipio_id):
    try:
        eliminar = Municipio.objects.get(pk=municipio_id)
        if request.method == 'POST':
            eliminar.delete()
            return redirect('catalogos')  # Redirige a la lista de municipios
        return render(request, 'catalogos/eliminar.html', {'municipio': eliminar})
    except Municipio.DoesNotExist:
        return redirect('catalogos')