from django.contrib.auth.views import LogoutView
from django.urls import path

from . import views
from catalogos.views import PaisView, DepartamentoView, MunicipioView

urlpatterns = [
    path('catalogos/', views.catalogos, name='catalogos'),

    path('crear_pais/', views.crear_pais, name='crear_pais'),
    path('crear_departamento/', views.crear_departamento, name='crear_departamento'),
    path('crear_municipio/', views.crear_municipio, name='crear_municipio'),

    path('eliminar_pais/<int:pais_id>/', views.eliminar_pais, name='eliminar_pais'),
    path('eliminar_departamento/<int:departamento_id>/', views.eliminar_departamento, name='eliminar_departamento'),
    path('eliminar_municipio<int:municipio_id>/', views.eliminar_municipio, name='eliminar_municipio'),

    path('editar_pais/<int:pais_id>/', views.editar_pais, name='editar_pais'),
    path('editar_departamento/<int:departamento_id>/', views.editar_departamento, name='editar_departamento'),
    path('editar_municipio/<int:municipio_id>/', views.editar_municipio, name='editar_municipio'),

]
