from rest_framework import serializers
from .models import Pais, Departamento, Municipio  # Asegúrate de que esta importación refleje la ubicación real de tu modelo Pais

class PaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pais  # Especifica el modelo que deseas serializar
        fields = '__all__'  # Puedes listar los campos que deseas incluir o '__all__' para incluir todos los campos

class DepartamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamento  # Especifica el modelo que deseas serializar
        fields = '__all__'  # Puedes listar los campos que deseas incluir o '__all__' para incluir todos los campos


class MunicipioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipio  # Especifica el modelo que deseas serializar
        fields = '__all__'  # Puedes listar los campos que deseas incluir o '__all__' para incluir todos los campos
